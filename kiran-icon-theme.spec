Name:		kiran-icon-theme
Version:	2.6.0
Release:	3
Summary:	Icon theme for Kiran desktop

License:	LGPL-2.0-or-later
URL:            http://www.kylinos.com.cn
Source0:        %{name}-%{version}.tar.gz

BuildRequires:  cmake

Requires:	google-noto-cjk-fonts
BuildArch:      noarch

Conflicts:      kiran-themes < 0.8.1

%description
The kiran-icon-theme package contains the standard icon theme for the Kiran
desktop, which provides default appearance for icons.


%prep
%setup -q

%build
%cmake
%cmake_build

%install
%cmake_install

%transfiletriggerin -- %{_datadir}/icons/Kiran
gtk-update-icon-cache --force %{_datadir}/icons/Kiran &>/dev/null || :

%transfiletriggerpostun -- %{_datadir}/icons/Kiran
gtk-update-icon-cache --force %{_datadir}/icons/Kiran &>/dev/null || :

%transfiletriggerin -- %{_datadir}/icons/Summer
gtk-update-icon-cache --force %{_datadir}/icons/Summer &>/dev/null || :

%transfiletriggerpostun -- %{_datadir}/icons/Summer
gtk-update-icon-cache --force %{_datadir}/icons/Summer &>/dev/null || :

%transfiletriggerin -- %{_datadir}/icons/Spring
gtk-update-icon-cache --force %{_datadir}/icons/Spring &>/dev/null || :

%transfiletriggerpostun -- %{_datadir}/icons/Spring
gtk-update-icon-cache --force %{_datadir}/icons/Spring &>/dev/null || :

%files
%{_datadir}/icons/Kiran/*
%{_datadir}/icons/Summer/*
%{_datadir}/icons/Spring/*

%changelog
* Thu Nov 14 2024 Funda Wang <fundawang@yeah.net> - 2.6.0-3
- adopt to new cmake macro
- add file triggers for gkt-icon-cache

* Tue Apr 09 2024 yuanxing <yuanxing@kylinsec.com.cn> - 2.6.0-2
- KYOS-B: add icon of gnome software with correct icon name in Kiran.(#28021)
- KYOS-B: update icons to smaller size to fix the slowly problem when open caja.(#33978)

* Wed Nov 29 2023 yuanxing <yuanxing@kylinsec.com.cn> - 2.6.0-1
- KYOS-F: rename KiranNew to Summer,improve icons and add Spring icon theme.(#20205)

* Sat Jul 08 2023 kpkg <kpkg.kylinsec.com.cn> - 2.5.0-2.kb1
- rebuild for KiranUI-2.5-next

* Fri Jun 09 2023 yuanxing <yaunxing@kylinsec.com.cn> - 2.5.0-2
- KYOS-F: add some KiranNew icons and remove commercial icons

* Tue Mar 28 2023 yuanxing <yaunxing@kylinsec.com.cn> - 2.5.0-1
- KYOS-F: init commit for v2.5 version.

